/*
Xtal is a command line app for managing version control 
systems with a focus on usability. 
*/
#include <assert.h>
#include <libgen.h>
#include <sqlite3.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

/***********************************
*
*
*    Utilities
*
*
***********************************/


/* Log an informational message to stdout. */
void log_info(const char* s) {
  printf("\n(^_^)  %s\n\n", s);
}

/* Log a message about a successful action to stdout. */
void log_success(const char* s) {
  printf("\n(^o^)  %s\n\n", s);
}

/* Log an error message to stdout. */
void log_error(const char* s) {
  printf("\n(>.<)  ERROR! %s\n\n", s);
}

/*
Get the type of SCM of the current directory.
*/
const char* cwd_scm() {
  char path[1000];
  getcwd(path, 1000);
  char* name = basename(path);
  char fossil[1000];
  // We assume a .fossil file with the same name as cwd.
  sprintf(fossil, "%s.fossil", name);

  if (access(".git", F_OK) == 0) {
    return "git";
  } else if (access(fossil, F_OK) == 0) {
    return "fossil";
  } else {
    return NULL;
  }
}

/* 
Get the path of the database. 
Will exit the program if HOME is not set.
*/
char* db_path() {
  char* home = getenv("HOME");
  char* file = "/.xtal";
  char* path = malloc(strlen(home) + strlen(file) + 1);

  if (home == NULL) {
    log_error("your HOME environment variable is not set.");
    exit(EXIT_FAILURE);
  }
  strcat(path, home);
  return strcat(path, file);
}

/*
Log a message at a given log level ("error" or "info") 
using a template and some variables. 
*/
void log_template(const char* level, const char* template, ...)
{
  va_list args;
  // Leave room in the buffer for the template and variables.
  char* text = malloc(strlen(template) + 1000);

  va_start(args, template);
  vsprintf(text, template, args);
  va_end(args);

  if (strcmp(level, "error") == 0) {
    log_error(text);
  } else if (strcmp(level, "info") == 0) {
    log_info(text);
  } else if (strcmp(level, "success") == 0) {
    log_success(text);
  } else {
    exit(EXIT_FAILURE);
  }
}

/*
Check that the command was given the correct 
number of arguments.
*/
void check_command_argc(const char* command, int argc, int correct) {
  // Use different phrasing for one or multiple arguments.
  char* template = "";
  if (correct == 0) {
    if (argc != correct) {
      log_template("error", 
        "\"%s\" doesn't accept any arguments.", command
      );
      exit(EXIT_FAILURE);      
    }
  } else {
    template = "\"%s\" must be given %d arguments.";
    if (argc != correct) {
      log_template("error", 
        "\"%s\" must be given %d arguments.", command, correct
      );
      exit(EXIT_FAILURE);      
    }
  }  
}

void invalid_command(char* command) {
  // If given an invalid command, display an error.
  char text[100];
  log_template("error", 
    "\"%s\" is not a valid command.", command
  );
  exit(EXIT_FAILURE);
}

void system_quiet(const char* command) {
  char full[200];
  sprintf(full, "%s > /dev/null 2>&1", command);
  assert(!system(full));
}

// Get the filename without the extension, or NULL if none.
const char* file_no_ext(const char* path) {
    char* file = basename(strdup(path));
    char* ext = strrchr(file, '.');

    if (!ext) {
      return NULL;
    }
    // Change the dot to a NULL terminator to cut the string.
    *ext = '\0';
    return file;
}

// Get the file extension from a path, or NULL if none.
const char* file_ext(const char* path) {
    char* file = basename(strdup(path));
    char* ext = strrchr(file, '.');

    // Return everything after the last dot.
    return strdup(ext + 1);
}

/***********************************
*
*
*    SQLite
*
*
***********************************/

// void sql_query(sqlite3* db, const char* sql) {
// }

sqlite3_stmt* sql_compile(sqlite3* db, const char* sql) {
  sqlite3_stmt* stmt;
  assert(!sqlite3_prepare_v2(db, sql, -1, &stmt, NULL));
  return stmt;
}

// Execute SQL that doesn't return a value.
void sql_command(sqlite3* db, const char* sql) {
  sqlite3_stmt* stmt = sql_compile(db, sql);
  assert(SQLITE_DONE == sqlite3_step(stmt));
  sqlite3_finalize(stmt);  
}

// Bind any number of text parameters terminated by NULL. 
void sql_bind_text(sqlite3_stmt* stmt, int count, ...) {
  va_list args;
  const char* text;
  int i = 1;

  va_start(args, count);
  for (; i <= count ; i++) {
    text = va_arg(args, const char*);
    assert(!sqlite3_bind_text(stmt, i, text, -1, NULL));
  }
  va_end(args);
}

sqlite3* db_open() {
  sqlite3* db;
  const char* path;
  path = db_path();

  // Open the existing database if it exists.
  if (access(path, F_OK) == 0) {
    assert(!sqlite3_open_v2(
      path, &db, SQLITE_OPEN_READWRITE, NULL
    ));
  } else {
    assert(!sqlite3_open_v2(
      path, &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, NULL
    ));
    // Create the database and schema if it doesn't exist.
    sql_command(db, "create table repos "
      "(id integer primary key, path unique, name unique, scm);"
    );
  }
  return db;
}

/***********************************
*
*
*    Help
*
*
***********************************/

// Get general help text about xtal. 
void help() {
  const char* template = "xtal v%s\n"
    "\n"
    "Xtal is a command line app for managing version control\n"
    "systems with a focus on usability.\n"
    "\n"
    "--- Global commands (works in any directory):\n"
    "\n"
    "xtal help [command]     - display help for a command\n"
    "xtal new [scm] [name]   - create and manage a new local repo\n"
    "xtal clone [url]        - clone and manage a remote repo\n"
    "xtal own                - make xtal manage the current repo\n"
    "xtal all                - list all managed repos\n"
    "xtal disown [name]      - remove the current repo from xtal\n"
    "\n"
    "--- Local repo commands (must be in repo's directory):\n"
    "\n"
    "xtal log                - display a log of recent revisions\n"
    "xtal info               - get info on the state of the repo\n"
    "xtal diff               - display the repo's unsaved changes\n"
    "xtal save               - review and save a new revision\n"
    "xtal link [name] [url]  - link a remote repo to sync with\n"
    "xtal unlink [name]      - unlink a remote repo";
  log_template("info", template, XTAL_VERSION);
}

void help_new() {
  log_info("help for \"xtal new [scm] [name]\":\n"
    "\n"
    "This command creates a new local repo using the given\n"
    "SCM (git or fossil) and makes xtal manage it.\n"
    "\n"
    "If you already have a local repo and you want xtal\n"
    "to manage it, just \"cd\" into the repo's directory\n"
    "and run \"xtal own\" instead."
  ); 
}

void help_clone() {
  log_info("help for \"xtal clone [url]\":\n"
    "\n"
    "This command clones a remote repo locally and\n"
    "automatically links the remote repo for syncing."
  ); 
}

void help_own() {
  log_info("help for \"xtal own\":\n"
    "\n"
    "If you have a repository already and you want\n"
    "xtal to manage it, \"cd\" into the project\n"
    "directory and run \"xtal own\" to make xtal\n"
    "own the project.\n"
    "\n"
    "Xtal will automatically determine the type of\n"
    "version control system used for the project."
  );
}

void help_disown() {
  log_info("help for \"xtal disown [name]\":\n"
    "\n"
    "This command removes a repo from xtal's management."
  );
}

void help_all() {
  log_info("help for \"xtal all\":\n"
    "\n"
    "This command lists the status of all repos\n"
    "managed by xtal.\n"
    "\n"
    "It works the same from any directory."
  );
}

void help_log() {
  log_info("help for \"xtal log\":\n"
    "\n"
    "This command displays a log of recently saved revisions."
  ); 
}

void help_info() {
  log_info("help for \"xtal info\":\n"
    "\n"
    "This command displays information about the state\n"
    "of the current repo."
  ); 
}

void help_diff() {
  log_info("help for \"xtal diff\":\n"
    "\n"
    "This command displays the unsaved changes\n"
    "in all of the current repo's files."
  ); 
}

void help_save() {
  log_info("help for \"xtal save\":\n"
    "\n"
    "This command lets you review the changes you've made,\n"
    "and save a new revision if it looks good."
  ); 
}

void help_link() {
  log_info("help for \"xtal link [name] [url]\":\n"
    "\n"
    "This command adds a remote repo that the local repo\n"
    "will be automatically synced with."
  );
}

void help_unlink() {
  log_info("help for \"xtal unlink [name]\":\n"
    "\n"
    "This command removes a remote repo and stops\n"
    "automatically syncing with it."
  );
}

/***********************************
*
*
*    Global Commands
*
*
***********************************/

void do_own(int argc, char* argv[]) {
  check_command_argc("own", argc, 0);

  const char* scm = cwd_scm();
  if (scm == NULL) {
    log_error("This directory doesn't look like a repo to me.");
    exit(EXIT_FAILURE);
  }

  sqlite3* db = db_open();
  sqlite3_stmt* stmt;
  char path[1000];
  getcwd(path, 1000);
  char* name = basename(path);

  // Add an entry in the xtal db for this project.
  stmt = sql_compile(db, "insert into repos "
    "(path, name, scm) values (?1, ?2, ?3);"
  );
  sql_bind_text(stmt, 3, path, name, scm);

  int result = sqlite3_step(stmt);
  if (result == SQLITE_CONSTRAINT) {
    log_template("error", 
      "Xtal already owns a repo with this name or path.\n"
      "\n"
      "Try running \"xtal all\".", scm
    );
  } else {
    assert(result == SQLITE_DONE);
    log_template("success", 
      "Successfully owned the %s repo at %s !", scm, path
    );
  }
  sqlite3_finalize(stmt);
  assert(!sqlite3_close_v2(db));
}

void do_disown(int argc, char* argv[]) {
  check_command_argc("disown", argc, 1);

  sqlite3* db = db_open();
  sqlite3_stmt* stmt;
  char* name = argv[1];

  stmt = sql_compile(db, "delete from repos where name=?1");
  sql_bind_text(stmt, 1, name);

  int result = sqlite3_step(stmt);
  if (result != SQLITE_DONE) {
    log_template("error", "Xtal couldn't disown %s.", name);
    exit(EXIT_FAILURE);
  }

  log_template("success", "Successfully disowned %s.", name);
  sqlite3_finalize(stmt);
  assert(!sqlite3_close_v2(db));
}

void do_new(int argc, char* argv[]) {
  check_command_argc("new", argc, 2);

  const char* scm = argv[1];
  const char* name = argv[2];
  char command[100];

  // Make a new directory for the repo then initialize it.
  if (strcmp(scm, "git") == 0) {
    mkdir(name, 0777);
    chdir(name);
    system_quiet("git init");
  } else if (strcmp(scm, "fossil") == 0) {
    sprintf(command, "fossil init %s.fossil", name);
    mkdir(name, 0777);
    chdir(name);
    system_quiet(command);
    // With Fossil you need to explicitly open the repo.
    sprintf(command, "fossil open %s.fossil", name);
    system_quiet(command);
  } else {
    log_template("error", 
      "I don't understand \"%s\" as an SCM.\n"
      "\n"
      "I only know the SCMs git and fossil."
    );
  }
  // Make xtal own the new repo.
  system_quiet("xtal own");
  log_template("success", 
    "Successfully created a %s repo named %s.", scm, name
  );
}

void do_clone(int argc, char* argv[]) {
  check_command_argc("clone", argc, 1);
  
  const char* url = argv[1];
  const char* name = file_no_ext(url);
  const char* ext = file_ext(url);
  char clone[300];

  if (strcmp(ext, "git") == 0) {
    log_template("info", "Cloning the %s git repo for you...", name);
    sprintf(clone, "git clone %s %s", url, name);
    system_quiet(clone);
    assert(!chdir(name));
    system_quiet("xtal own");
    log_template("success", 
      "Successfully cloned and owned the %s repo!", name
    );
  } else if (strcmp(ext, "fossil") == 0) {
    log_template("info", "Cloning the %s fossil repo for you...", name);
    // For fossil, we make a directory, clone, then open the repo.
    assert(!mkdir(name, 0777));
    assert(!chdir(name));
    sprintf(clone, "fossil clone %s %s.fossil", url, name);
    system_quiet(clone);
    char open[300];
    sprintf(open, "fossil open %s.fossil", name);
    system_quiet(open);
    system_quiet("xtal own");
    log_template("success", 
      "Successfully cloned and owned the %s repo!", name
    );
  } else {
    log_error("I can only clone URLs ending in \".git\" or \".fossil\"");
  }
}

void do_all(int argc, char* argv[]) {
  check_command_argc("all", argc, 0);

  log_info("Here's some info on of all your repos:");

  sqlite3* db = db_open();
  int num_repos = 0;
  // Save the current directory so we can return to it.
  char old_dir[1000];
  getcwd(old_dir, 1000);

  // Print the status of each project. 
  sqlite3_stmt* stmt = sql_compile(db, 
    "select path, name, scm from repos "
    "order by scm asc, name asc;"
  );
  for (int rc = sqlite3_step(stmt); rc != SQLITE_DONE; rc = sqlite3_step(stmt)) {
    num_repos++;
    const char* path = sqlite3_column_text(stmt, 0);
    const char* name = sqlite3_column_text(stmt, 1);
    const char* scm = sqlite3_column_text(stmt, 2);
    printf("__%d__ %s (%s) @ %s\n", num_repos, name, scm, path);
    chdir(path);
    if (strcmp(scm, "git") == 0) {
      system("git status");
    } else if (strcmp(scm, "fossil") == 0) {
      system_quiet("fossil addremove");
      system("fossil changes");
    }
    printf("\n");
  }
  sqlite3_finalize(stmt);

  if (num_repos == 0) {
    log_error("Xtal doesn't currently manage any repos.\n"
      "\n"
      "You can use \"xtal own\" to manage an existing repo\n"
      "or \"xtal new\" to create and manage a new repo.\n"
      "\n"
      "Use \"xtal help\" to get more info."
    );
  }

  assert(!sqlite3_close_v2(db));

  // Return to the original directory. 
  chdir(old_dir);
}

/***********************************
*
*
*    Git Commands
*
*
***********************************/

void git_log(int argc, char* argv[]) {
  check_command_argc("log", argc, 0);
  system("git log");
}

void git_info(int argc, char* argv[]) {
  check_command_argc("info", argc, 0);

  log_info("Syncing the repo to put your changes in front...");
  system_quiet("git pull");

  log_info("Here's some info about the state of this repo:");
  system("git status");
}

void git_diff(int argc, char* argv[]) {
  check_command_argc("diff", argc, 0);
  log_info("Here are the unsaved changes in this repo:");
  system("git diff");
}

void git_save(int argc, char* argv[]) {
  check_command_argc("save", argc, 0);

  log_info("Syncing the repo to put your changes in front...");
  system_quiet("git pull");

  log_info("Review your changes and see if they look good:");
  system("git diff");

  log_info(
    "Enter a one line summary of your changes: (Ctrl-C to cancel)"
  );

  // Prompt for commit message
  int bytes_read;
  unsigned long len = 300;
  char *msg;
  msg = (char *) malloc(len + 1);
  bytes_read = getline(&msg, &len, stdin);

  if (bytes_read == -1) {
    // If nothing was entered, don't save anything.
    log_info("Ok, xtal won't save your changes just yet.");
    exit(EXIT_FAILURE);
  }

  // Add and commit the changes. 
  system_quiet("git add -A .");
  char commit[strlen(msg) + 20];
  sprintf(commit, "git commit -m \"%s\"", msg);
  system_quiet(commit);
  log_success("Successfully saved your changes! Syncing them...");

  system_quiet("git push");
}

void git_link(int argc, char* argv[]) {
  check_command_argc("link", argc, 2);
}

void git_unlink(int argc, char* argv[]) {
  check_command_argc("unlink", argc, 1);
}

/***********************************
*
*
*    Fossil Commands
*
*
***********************************/

void fossil_log(int argc, char* argv[]) {
  check_command_argc("log", argc, 0);
  system("fossil timeline");
}

void fossil_info(int argc, char* argv[]) {
  check_command_argc("info", argc, 0);

  log_info("Syncing the repo to put your changes in front...");
  system_quiet("fossil update");

  log_info("Here's some info about the state of this repo:");
  // First add the changes so Fossil shows them.
  system_quiet("fossil addremove");
  system("fossil changes");
}

void fossil_diff(int argc, char* argv[]) {
  check_command_argc("diff", argc, 0);
  log_info("Here are the unsaved changes in this repo:");
  system_quiet("fossil addremove");
  system("fossil diff --tk");
}

void fossil_save(int argc, char* argv[]) {
  check_command_argc("save", argc, 0);

  log_info("Syncing the repo to put your changes in front...");
  system_quiet("fossil addremove");
  system_quiet("fossil update");

  log_info("Review your changes and see if they look good:");
  system("fossil diff --tk");

  log_info(
    "Enter a one line summary of your changes: (Ctrl-C to cancel)"
  );

  // Prompt for commit message
  int bytes_read;
  unsigned long len = 300;
  char *msg;
  msg = (char *) malloc(len + 1);
  bytes_read = getline(&msg, &len, stdin);

  if (bytes_read == -1) {
    // If nothing was entered, don't save anything.
    log_info("Ok, xtal won't save your changes just yet.");
    exit(EXIT_FAILURE);
  }

  // Add and commit the changes. 
  system_quiet("fossil addremove");
  char commit[strlen(msg) + 20];
  sprintf(commit, "fossil commit -m \"%s\"", msg);
  system_quiet(commit);
  log_success("Successfully saved your changes! Syncing them...");
}

void fossil_link(int argc, char* argv[]) {
  check_command_argc("link", argc, 2);
}

void fossil_unlink(int argc, char* argv[]) {
  check_command_argc("unlink", argc, 1);
}

/***********************************
*
*
*    Main
*
*
***********************************/

int main(int argc, char* argv[]) {
  char* command = argv[1];
  struct global_command_list {
    char *name;
    void (*callback)(int argc, char *argv[]);
    void (*help)();
  } global_commands[] = {
    "new", do_new, help_new,
    "clone", do_clone, help_clone,
    "own", do_own, help_own,
    "disown", do_disown, help_disown,
    "all", do_all, help_all,
    NULL, NULL, NULL
  };

  struct local_command_list {
    char *name;
    void (*git)(int argc, char *argv[]);
    void (*fossil)(int argc, char *argv[]);
    void (*help)();
  } local_commands[] = {
    "log", git_log, fossil_log, help_log,
    "info", git_info, fossil_info, help_info,
    "diff", git_diff, fossil_diff, help_diff,
    "save", git_save, fossil_save, help_save,
    "link", git_link, fossil_link, help_link,
    "unlink", git_unlink, fossil_unlink, help_unlink,
    NULL, NULL, NULL
  };
  int i;

  // Passing no arguments displays the help text.
  if (argc == 1) {
    help();
    exit(EXIT_SUCCESS);
  }
  // Process the help command.
  if (strcmp(command, "help") == 0) {
    if (argc != 3) {
      help();
      exit(EXIT_SUCCESS);
    }
    // Check all local functions. 
    for (i = 0; local_commands[i].name != NULL; i++) {
      // Execute the help of the given command.
      if (strcmp(argv[2], local_commands[i].name) == 0) {
        local_commands[i].help();
        exit(EXIT_SUCCESS);
      }
    }
    // Check global functions. 
    for (i = 0; global_commands[i].name != NULL; i++) {
      // Execute the help of the given command.
      if (strcmp(argv[2], global_commands[i].name) == 0) {
        global_commands[i].help();
        exit(EXIT_SUCCESS);
      }
    }
    invalid_command(argv[2]);
  }
  // Check if the command is a global command first.
  for (i = 0; global_commands[i].name != NULL; i++) {
    // If the command name matches, execute the callback function.
    if (strcmp(command, global_commands[i].name) == 0) {
      global_commands[i].callback(argc - 2, argv + 1);
      exit(EXIT_SUCCESS);      
    }
  }
  for (i = 0; local_commands[i].name != NULL; i++) {
    // Execute the SCM function appropriate for this directory.
    if (strcmp(command, local_commands[i].name) == 0) {
      const char* scm = cwd_scm();
      // Pass the rest of the arguments to the command. 
      if (strcmp(scm, "git") == 0) {
        local_commands[i].git(argc - 2, argv + 1);
        exit(EXIT_SUCCESS);
      } else if (strcmp(scm, "fossil") == 0) {
        local_commands[i].fossil(argc - 2, argv + 1);
        exit(EXIT_SUCCESS);
      } else {
        log_error("This directory doesn't look like an SCM repo.");
        exit(EXIT_FAILURE);
      }
    }
  }
  invalid_command(command);
}
