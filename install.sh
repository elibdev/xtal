#!/bin/sh

OLD_DIR=$PWD

# Create a build directory if it doesn't exist.
mkdir -p build

# Build from the source dir, then return to the original dir.
cd build
cmake ..
make
make install
cd $OLD_DIR