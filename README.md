# Xtal

Xtal is a command line app for managing version control systems with a focus on usability. 

The goal is to offer a uniform interface to version control systems like [Git](https://git-scm.com) and [Fossil](https://www.fossil-scm.org) and ease management of multiple repositories. 

## How to Install

Xtal is currently a work in progress, so don't count on it working reliably yet. 

1. Clone this repo. 

2. [Install CMake.](https://cmake.org/install/)

3. Run `./install.sh`. If that doesn't work, try running `chmod +x install.sh` to make the script executable, or `sudo ./install.sh` if you need root permissions. 

You can then run `xtal` to get usage information. 

## Auto-sync and Multi-Repo Functionality

Xtal introduces some additional functionality to the typical version control workflow for the user's convenience. 

Xtal automatically adds all file changes and automatically synchronizes with remote repos before and after saving a revision. This encourages merging your code frequently to prevent merge conflicts and stay up-to-date with other new changes.

Xtal also offers the `xtal all` command that synchronizes and prints the status of all repositories that Xtal manages. This is enabled by maintaining an SQLite database file at `~/.xtal`. Every time you run `xtal new`, `xtal copy` or `xtal own` `~/.xtal` is updated automatically to manage the current repo. 

## A Basic Workflow with Xtal

1. Run `xtal new` to create a new repo, `xtal copy` to copy an existing remote repo locally, or `xtal own` in the directory of an existing local repo. 

2. Run `xtal info` to get information about the current state of the repo. Make some changes to the version controlled files. 

3. Run `xtal save` to review and save your changes as a new version.

add them to the commit, automatically synchronize with remote repos, and make the commit